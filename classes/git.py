import re
import os
import subprocess

from classes.scm import Scm


class Git(Scm):

    def get_source_directory(self):
        return self.project_dir

    def get_marks_file(self, branch):
        return os.path.join(self.get_marks_directory(), '%s-git' % branch)

    def branch_exists(self, branch):
        """

        :type branch: string
        """
        matcher = re.compile(r'\s%s$' % branch)
        branches = self.run_command(['git', 'branch', '-a'], return_output=True).decode().split('\n')
        for x in branches:
            if matcher.search(x):
                return True
        return False

    def init(self):
        os.mkdir(self.project_name)
        return self.run_command(['git', 'init'])

    def log(self, branch, args=None):
        if args is None:
            args = []
        args.append(branch)
        return self.run_command(['git', 'log'] + args)

    def checkout(self, branch_name):
        return self.run_command(['git', 'checkout', branch_name])

    def clean(self):
        self.run_command(['git', 'reset', '--hard']).wait()
        self.run_command(['git', 'clean', '-fd']).wait()

    def branch_delete(self, branch_name):
        return self.run_command(['git', 'branch', '-D', branch_name])

    def fast_import(self, args, stdin=subprocess.PIPE):
        return self.run_command(['git', 'fast-import'] + args, stdin=stdin)

    def branch_from_bzr(self, bzr_branch):
        my_bzr_branch = bzr_branch[3:].lstrip('~')
        if my_bzr_branch == self.project_name:
            return 'master'
        else:
            return re.sub(
                r'^' + self.project_group + '/' + self.project_name,
                '',
                my_bzr_branch
            ).lstrip('/').replace('/' + self.project_name + '/', '/')
