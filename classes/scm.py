import logging
import os
import subprocess
from abc import abstractmethod


class Scm:
    def __init__(self, project_dir, cache_dir, project_name, project_group=''):
        self.project_dir = project_dir
        self.cache_dir = cache_dir
        self.project_name = project_name
        self.project_group = project_group

    def die(self, message, *args):
        logging.error(message, *args)
        raise Exception(message)
        # sys.exit(1)

    def get_source_directory(self):
        return self.project_dir

    def get_marks_directory(self):
        return os.path.join(self.cache_dir, 'map', self.project_name)

    @abstractmethod
    def get_marks_file(self, branch):
        raise NotImplementedError

    def clear_marks_file(self, branch):
        if os.path.exists(self.get_marks_file(branch)):
            return os.remove(self.get_marks_file(branch))

    def run_command(self, cmd, error_ok=False, error_message=None, exit_code=False,
                    return_output=False, stdout=subprocess.PIPE, stdin=None,
                    cwd=None):
        # Useful for debugging:
        logging.debug(' '.join(cmd))

        if cwd is None:
            cwd = self.get_source_directory()

        process = subprocess.Popen(cmd, stdout=stdout, stdin=stdin, cwd=cwd)

        if not return_output:
            return process

        if stdout == subprocess.PIPE:
            output = process.communicate()[0]
        else:
            output = ''
            process.wait()

        if exit_code:
            return process.returncode
        if not error_ok and process.returncode != 0:
            self.die('Command "%s" failed.\n' % (' '.join(cmd)) + (error_message or output))
        return output
