import os
import shutil

from classes.scm import Scm


class Bzr(Scm):

    def get_source_directory(self, branch_name=''):
        return os.path.join(Scm.get_source_directory(self), self.project_name, branch_name)

    def get_marks_file(self, branch):
        return os.path.join(self.get_marks_directory(), '%s-bzr' % self.branch_name(branch))

    def checked_out(self, branch):
        return os.path.exists(self.get_source_directory(self.branch_name(branch)))

    def delete(self, branch):
        shutil.rmtree(self.get_source_directory(self.branch_name(branch)))

    def pull(self, branch):
        return self.run_command(['bzr', 'pull', branch], cwd=self.get_source_directory(self.branch_name(branch)))

    def branch(self, branch):
        return self.run_command(['bzr', 'branch', branch])

    def log(self, branch, args=None):
        if args is None:
            args = []
        return self.run_command(['bzr', 'log'] + args, cwd=self.get_source_directory(self.branch_name(branch)))

    def fast_export(self, args, branch):
        return self.run_command(['bzr', 'fast-export', '--plain'] + args,
                                cwd=self.get_source_directory(self.branch_name(branch)))

    def clean(self, branch):
        return self.revert(branch)

    def revert(self, branch, file=None):
        cmd = ['bzr', 'revert']
        if file is not None:
            cmd.append(file)
        return self.run_command(cmd, cwd=self.get_source_directory(self.branch_name(branch)))

    # Get the owner and branch name.
    def branch_name(self, bzr_branch):
        return bzr_branch[3:].lstrip('~').split('/')[-1]

    def branch_exists(self, branch):
        return self.checked_out(branch)
