#!/usr/bin/env python
import argparse
import os
from launchpadlib import launchpad

from classes.git import Git
from classes.bzr import Bzr


# bzr branch to get the actual bzr repo.
def pull_bzr_branches(branch_name, bzr):
    def pp(proc):
        while proc.poll() is None:
            print(proc.stdout.readline().decode())
        print(proc.stdout.read().decode())
        return proc

    # Get the branch name.
    if bzr.checked_out(branch_name):
        print('Updating bzr code.')
        bzr.clean(branch_name).wait()
        p = pp(bzr.pull(branch_name))
    else:
        print('Checking out bzr code.')
        p = pp(bzr.branch(branch_name))

    if p.returncode:
        print('Cleaning up broken branch and trying again.')
        bzr.delete(branch_name)
        pp(bzr.branch(branch_name))
        bzr.clear_marks_file(branch_name)


def import_git_branch(git, bzr, branch_name):
    if bzr.checked_out(branch_name):
        git_branch_name = git.branch_from_bzr(branch_name)
        print('Importing: ' + branch_name + ' -> ' + git_branch_name)

        bzr_marks = bzr.get_marks_file(branch_name)
        git_marks = git.get_marks_file(git_branch_name)
        git_import_arg = [
            '--export-marks=%s' % git_marks,
        ]
        bzr_import_arg = [
            '--git-branch=%s' % git_branch_name,
            '--export-marks=%s' % bzr_marks,
        ]
        if os.path.exists(bzr_marks):
            bzr_import_arg.append('--import-marks=%s' % bzr_marks)
        if os.path.exists(git_marks):
            git_import_arg.append('--import-marks=%s' % git_marks)

        # bzr fast-export . | git fast-import
        bzr_proc = bzr.fast_export(bzr_import_arg, branch_name)
        git_proc = git.fast_import(git_import_arg, stdin=bzr_proc.stdout)
        bzr_proc.wait()
        git_proc.wait()


def main(project_name='lp:mg5amcnlo', branch_name=None):
    project_group = 'maddevelopers'
    cache_dir = os.path.abspath('./cachedir')
    try:
        os.mkdir(cache_dir)
        os.mkdir(os.path.join(cache_dir, 'bzr'))
    except OSError:
        pass

    if project_name.startswith('lp:'):
        project_name = project_name[3:]

    try:
        os.mkdir(os.path.join(cache_dir, 'bzr', project_name))
        os.mkdir(os.path.join(cache_dir, 'map', project_name))
    except OSError:
        pass

    git = Git(os.path.abspath('./' + project_name), cache_dir, project_name, project_group)
    bzr = Bzr(os.path.join(cache_dir, 'bzr'), cache_dir, project_name, project_group)

    # do initial setup
    if not os.path.exists(project_name):
        git.init()

    failures = []
    if branch_name is None:
        lp = launchpad.Launchpad.login_anonymously('madg_lp_to_git', 'production', cache_dir)
        project = lp.projects[project_name]
        for branch in project.getBranches():
            failures.append(import_branch(git, bzr, branch.bzr_identity))
    else:
        failures.append(import_branch(git, bzr, branch_name))

    for failure in filter(None, failures):
        print('Error importing ' + failure[0] + ': ')
        print(failure[1])


def import_branch(git, bzr, branch_name):
    print('Importing ' + branch_name)
    pull_bzr_branches(branch_name, bzr)

    try:
        if git.branch_exists(git.branch_from_bzr(branch_name)):
            print('Updating git branch')
            import_git_branch(git, bzr, branch_name)
        else:
            print('Creating git branch')
            import_git_branch(git, bzr, branch_name)
    except Exception as e:
        return [branch_name, e]


if __name__ == '__main__':
    parser = argparse.ArgumentParser("Import MadTeam migrations")
    parser.add_argument('repo', metavar='Repository', type=str, nargs='?', default="lp:mg5amcnlo",
                        help='The is the main launchpad repository for the project')
    parser.add_argument('--fork', type=str, nargs="?",
                        help='A specific fork/branch to import.')
    args = parser.parse_args()
    main(project_name=args.repo, branch_name=args.fork)
