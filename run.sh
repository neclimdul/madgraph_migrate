#!/bin/zsh

pipenv sync
pipenv run python import.py lp:mg5amcnlo &> madgraph_audit.log
pipenv run python test.py lp:mg5amcnlo

pipenv run python import.py lp:maddm &> maddm_audit.log
pipenv run python test.py lp:maddm
