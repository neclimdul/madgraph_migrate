#!/usr/bin/env python
import argparse
import os
import re
import subprocess
import time

from launchpadlib import launchpad

from classes.bzr import Bzr
from classes.git import Git


def compare_logs(git, bzr, branch):
    """
    Compare logs between git a bzr branches.

    This is suppose to compare branches but the regardless of how hard you try
    to massage git's logs the log order shows up different and small things are
    different making this kinda useless for a real test. Only sort of a review thing...
    """

    git_branch_name = git.branch_from_bzr(branch.bzr_identity)
    print('Testing log for:' + git_branch_name)
    bzr_proc = bzr.log(branch.bzr_identity, [
        '--gnu-changelog'
    ])
    git_proc = git.log(git_branch_name, [
        '--date=format:%Y-%m-%d',
        '--pretty=format:%ad  %an  <%ae>%n%n%x09%s%n',
    ])
    bzr_log = bzr_proc.stdout.read()
    git_log = git_proc.stdout.read()
    git_log = re.sub(
        b'. <>\n',
        b'\n',
        git_log
    )
    bzr_log_file = open('bzr.log', 'w+')
    bzr_log_file.write(bzr_log)
    git_log_file = open('git.log', 'w+')
    git_log_file.write(git_log)
    bzr_log_file.close()
    git_log_file.close()

    # Remove empty directories because they won't exist in get and make the review harder.
    f = open(os.path.join('logs', git_branch_name.replace('/', '-')) + ".log.diff", 'w+')
    proc = subprocess.Popen(['diff', '-up', 'git.log', 'bzr.log'], stdout=f)
    proc.wait()


def compare_source(git, bzr, project_name, branch_name):
    git_branch_name = git.branch_from_bzr(branch_name)
    if not bzr.branch_exists(branch_name):
        print("Missing bzr branch for: " + branch_name)
        return False

    if not git.branch_exists(git_branch_name):
        print("Missing git branch for: " + git_branch_name)
        return False

    # Clean any outstanding changes so we know the checkout will work.
    git.clean()
    print("Checking out branch " + git_branch_name)
    r = git.checkout(git_branch_name).wait()
    # It seems like something weird might happen with git because sometimes the messages come out of order from
    # above checkout and the following messages and processes. Ths could cause incorrect diffs so... give it a smidge
    # more time to ensure git finishes.
    time.sleep(1)
    diff_file = os.path.join('logs', project_name, git_branch_name.replace('/', '-')) + ".diff"
    print('Writing branch diff to ' + diff_file)
    subprocess.run(['find',
                    bzr.get_source_directory(bzr.branch_name(branch_name)),
                    '-type', 'd',
                    '-empty',
                    '-delete'])
    f = open(diff_file, 'w+')
    proc = subprocess.Popen(['diff', '-upr', '--no-dereference',
                             '-x', '.git',
                             '-x', '.bzr',
                             git.get_source_directory(),
                             bzr.get_source_directory(bzr.branch_name(branch_name))],
                            stdout=f, stderr=f)
    proc.wait()


def main(project_name='lp:mg5amcnlo', branch_name=None):
    # Is this in launchpad or does it need to be an argument?
    project_group = 'maddevelopers'
    cache_dir = os.path.abspath('./cachedir')
    if project_name.startswith('lp:'):
        project_name = project_name[3:]

    try:
        os.mkdir(os.path.join('logs', project_name))
    except OSError:
        pass

    git = Git(os.path.abspath('./' + project_name), cache_dir, project_name, project_group)
    bzr = Bzr(os.path.join(cache_dir, 'bzr'), cache_dir, project_name, project_group)

    if branch_name is None:
        print('Fetching project information')
        lp = launchpad.Launchpad.login_anonymously('madg_lp_to_git', 'production', cache_dir)
        project = lp.projects[project_name]
        for branch in project.getBranches():
            # compare_logs(git, bzr, branch)
            compare_source(git, bzr, project_name, branch.bzr_identity)
    else:
        compare_source(git, bzr, project_name, branch_name)


if __name__ == '__main__':
    parser = argparse.ArgumentParser("Test MadTeam migrations")
    parser.add_argument('repo', metavar='Repository', type=str, nargs='?', default="lp:mg5amcnlo",
                        help='The is the main launchpad repository for the project')
    parser.add_argument('--fork', type=str, nargs="?",
                        help='A specific fork/branch to import.')
    args = parser.parse_args()
    main(project_name=args.repo, branch_name=args.fork)
