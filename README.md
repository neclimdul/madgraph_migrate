# What is this?

Scripts to migrate madgraph to git.

## Running a Migration
- Install bzr and install the fastimport plugin.
- `./run.sh`
- push changes

## Validating results
- `./test.py`